package com.simon.blog.dao;

import com.simon.blog.entity.CategoryInfo;
import com.simon.blog.entity.CategoryInfoExample;
import org.apache.ibatis.annotations.Insert;

import java.util.List;

public interface CategoryInfoMapper {
    int deleteByPrimaryKey(Long id);
    int insert(CategoryInfo record);

    int insertSelective(CategoryInfo record);

    List<CategoryInfo> selectByExample(CategoryInfoExample example);

    CategoryInfo selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(CategoryInfo record);

    int updateByPrimaryKey(CategoryInfo record);
}