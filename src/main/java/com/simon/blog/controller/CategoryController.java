package com.simon.blog.controller;


import com.simon.blog.entity.CategoryInfo;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/*
 *注解：
 *  ApiOperation用于说明该API的作用
 *  ApiImplicitParam用于说明该API的参数信息
 */
@Controller
@RequestMapping("/api/category")
public class CategoryController {


    @ApiOperation("获取所有的分类列表")
    @GetMapping("/list")
    public List<CategoryInfo> listAllCategoryInfo(){
        return  null;
    }

    @ApiOperation("获取某一条分类信息")
    @ApiImplicitParam(name = "id", value = "分类ID", required = true, dataType = "Long")
    @GetMapping("/{id}")
    public CategoryInfo getSortInfoById(@PathVariable Long id) {
        return null;
    }


    @ApiOperation("增加分类信息")
    @ApiImplicitParam(name = "name", value = "分类名称", required = true, dataType = "String")
    @PostMapping("/addCategory")
    public String addCategoryInfo() {
        return null;
    }


    @ApiOperation("更新/编辑分类信息")
    @ApiImplicitParam(name = "id", value = "分类ID", required = true, dataType = "Long")
    @PutMapping("/{id}")
    public String updateCategoryInfo(@PathVariable Long id) {
        return null;
    }

    @ApiOperation("删除分类信息")
    @ApiImplicitParam(name = "id", value = "分类ID", required = true, dataType = "Long")
    @DeleteMapping("/{id}")
    public String deleteCategoryInfo(@PathVariable Long id) {
        return null;
    }

}
