package com.simon.blog.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by DarLiu
 * <p>
 * On 2018/6/20
 */
public class IndexController {


    @ApiOperation("获取首页页面")
    @GetMapping("/")
    public String home(){
        return "index";
    }

    @ApiOperation("获取首页页面")
    @GetMapping("/index")
    public String index(){
        return "index";
    }




}
